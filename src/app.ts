import * as express from "express";
import * as path from "path";
import * as logger from "morgan";
import {Application, Request, Response, NextFunction} from "express";
import {IndexRouter} from "./routes";
import {Error} from "./entities/error";

const cookieParser = require("cookie-parser");
const createError = require("http-errors");
const mongoose = require("mongoose");
const cors = require('cors');

export class App {
    // express application
    readonly app: Application;

    /**
     * routing the app
     */
    private routing(): void {
        this.app.use("/", new IndexRouter().router);
    }

    /**
     * App getter
     * @returns {e.Application}
     */
    getApp(): Application {
        return this.app;
    }

    /**
     * Define the app
     * run middleware, routing and errorHandle
     */
    constructor() {
        this.app = express();
        mongoose.Promise = global.Promise;
        const url = 'mongodb://heroku_8ww9rtj8:lbcq9rou848mjur96u9ojf130r@ds125912.mlab.com:25912/heroku_8ww9rtj8';
        // const url = 'mongodb://localhost:27017/shopping-data'
        mongoose.connect(url,  { useNewUrlParser: true })
            .then(async () => {
                console.log("Connect to database success!");
            })
            .catch((err: any) => console.log(err));
        this.middleware();
        this.routing();
        this.handleError();
    }

    /**
     * Add middleware for the app
     */
    private middleware(): void {
        this.app.set("views", path.join(__dirname, "../views"));
        this.app.set("view engine", "pug");
        this.app.use(logger("dev"));
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(cookieParser());
        this.app.use(express.static(path.join(__dirname, "public")));
        this.app.use(cors());
        this.app.use((req: Request, res: Response, next: NextFunction) => {
            if (!res.locals) {
                res.locals = {};
            }
            next();
        });
    }

    /**
     * Handle the error
     */
    private handleError(): void {
        // catch 404 and forward to error handler
        this.app.use((req: Request, res: Response, next: NextFunction) => {
            next(createError(404));
        });

        this.app.use((err: any, req: Request, res: Response, next: any) => {
            // set locals, only providing error in development
            res.locals.message = err.message;
            res.locals.error = req.app.get("env") === "development" ? err : {};

            // render the error page
            res.status(err.status || 500);
            res.json({errors: {resource: new Error(err.status.toString(), err.message, err.message)}});
        });
    }
}