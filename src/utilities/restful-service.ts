import {MetaData} from "../entities/meta-data";
import {DataBaseError, NotFoundError, ConflictError} from "../entities/error";
import {Response} from "express";

export class RestfulService {
    private _hasMeta: boolean;
    private _query: any;
    private _type: string;

    get type(): string {
        return this._type;
    }

    set type(value: string) {
        this._type = value;
    }

    get hasMeta(): boolean {
        return this._hasMeta;
    }

    set hasMeta(value: boolean) {
        this._hasMeta = value;
    }

    get query(): any {
        return this._query;
    }

    set query(value: any) {
        this._query = value;
    }

    /**
     * return a new restful service with the configure param
     * @param type: <string> Type to response json, the name of the entity, or model,...
     * @param hasMeta: <boolean> Declare that response object has meta or not, if hasMeta = true, response object will have meta property
     * @param query: <any> If hasMeta = true, pass the req.query to this function, if not, query = false
     */
    static getInstance(type: string, hasMeta: boolean, query: any): RestfulService {
        const sv = new RestfulService();
        sv.hasMeta = hasMeta;
        sv.query = query;
        sv.type = type;
        return sv;
    }

    /**
     * - Normal response, auto read result of mongoose execute promise to make the response data
     * - If hasMeta = true, that the result is an promise of aggregate(), then read length of result[0].data to response
     * - If hasMeta = false, that the result is an promise of find(), then check null result to response
     * @param result
     * @param res
     */
    async doResponse(result: Promise<any>, res: Response): Promise<any> {
        try {
            const rs = await result;
            if (this.hasMeta) {
                // Condition if get list with 'noLimit' query
                if (rs.length !== 0 && !rs[0].meta) {
                    return res.json({
                        data: {
                            type: this.type,
                            attributes: rs
                        }
                    });
                }
                if (rs.length === 0 || rs[0].data.length === 0) {
                    return res.status(404).json({errors: {not_found: NotFoundError('Not found any results!')}});
                } else {
                    return res.json({
                        data: {
                            type: this.type,
                            attributes: rs[0].data
                        },
                        meta: new MetaData(rs[0].meta[0].total_items, this.query)
                    });
                }
            }
            else {
                // If not have any result
                if (rs === null || rs.length === 0) {
                    return res.status(404).json({errors: {not_found: NotFoundError('Not found any results!')}});
                }

                const responseData = <any> {
                    data: {
                        type: this.type
                    }
                }
                if (!rs.length) responseData.data.attribute = rs;
                else if (rs.length === 1) responseData.data.attribute = rs[0];
                else responseData.data.attributes = rs;
                res.json(responseData);
                
            }
        } catch (e) {
            this.responseError(e, res);
        }
    }

    /**
     * Response of transaction, use function and doc of mongoose transactions
     * https://www.npmjs.com/package/mongoose-transactions
     * If success: response the result
     * If error: Rollback transaction, clean it and response error
     * @param transaction
     * @param res
     */
    async doTransactionResponse(transaction: any, res: Response): Promise<any> {
        try {
            const rs = await transaction.run();
            res.json({
                data: {
                    type: this.type,
                    attributes: rs
                }
            });
        } catch (err) {
            const e = err.error;
            await transaction.rollback().catch(console.error);
            transaction.clean();
            this.responseError(e, res);
        }
    }

    /**
     * response mongoose error
     * If it's validate error, response with status 404
     * Status is 500 with MongoError
     * @param e
     * @param res
     */
    responseError(e: any, res: Response) {
        console.log('Error!', e);
        if (e.name === 'ValidationError') {
            if (e.error) {
                return res.status(403).json(e.error);
            }
            return res.status(403).json(e);
        }
        if (e.name === 'MongoError') {
            if (e.code === 11000) {
                return res.status(409).json({errors: {conflict: ConflictError(e.message)}});
            }
        }
        res.status(500).json({errors: {database: DataBaseError(e.message)}});
    }
}