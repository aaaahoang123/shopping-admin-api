
export class MongoosePipelineBuilder {
    private readonly _pipeline: any[] = [];

    get pipeline(): any[] {
        return this._pipeline;
    }

    constructor() {
        this._pipeline = [
            {
                $match: <any> {
                    status: 1
                }
            }
        ];
        return this;
    }

    match(document: any): MongoosePipelineBuilder {
        this._pipeline[0].$match = {...this._pipeline[0].$match, ...document};
        return this;
    }

    search(query: string, fields: string[] = []): MongoosePipelineBuilder {
        if (query) {
            let pattern = new RegExp(query, 'i');
            this._pipeline[0].$match.$or = [];
            fields.forEach(f => {
                let prop:any = {};
                prop[f] = pattern;
                this._pipeline[0].$match.$or.push(prop);
            });
        }
        return this;
    }

    paginate(condition: any = {}): MongoosePipelineBuilder {
        let limit = 10, skip = 0, page = 1;
        if (condition.limit && /^\d+$/.test(condition.limit)) limit = Math.abs(Number(condition.limit));
        // @ts-ignore
        if (condition.page && !['-1', '1'].includes(condition.page) && /^\d+$/.test(condition.page)) {
            page = Math.abs(Number(condition.page));
            skip = (page - 1) * limit;
        }
        if (!condition.noLimit) {
            this._pipeline.push(
                {
                    $facet: {
                        meta: [{$count: "total_items"}],
                        data: [{$skip: skip}, {$limit: limit}] // add projection here wish you re-shape the docs
                    }
                }
            );
        }
        return this;
    }
}