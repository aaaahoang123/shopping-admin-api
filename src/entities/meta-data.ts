export class MetaData {
    private total_items: number;
    private total_pages: number;
    private first: number;
    private last: number;
    private current_page: number;
    private limit: number;
    private q: string;
    private request_query: any


    constructor(total_items: number, query: any) {
        this.request_query = {...query};
        this.total_items = total_items;
        this.q = query.q;
        this.current_page = Number(query.page) || 1;
        this.limit = Number(query.limit) || 10;
        this.total_pages = Math.ceil(total_items/this.limit);
        this.first = this.limit*(this.current_page-1) + 1;
        const last = this.limit*this.current_page;
        this.last = last<total_items?last:total_items;
        
    }
}