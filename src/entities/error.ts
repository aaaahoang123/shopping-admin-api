export class Error {
    private code: string;
    private name: string;
    private message: string;

    constructor(code: string, name: string, message: string) {
        this.code = code;
        this.name = name;
        this.message = message;
    }
}

export const DataBaseError = (message: string) => new Error('500', 'Database Error', message);

export const NotFoundError = (message: string) => new Error('404', 'Not Found', message);

export const AuthenticationError = (message: string) => new Error('407', 'Authentication Error', message);

export const UnauthorizedError = (message: string) => new Error('401', 'Unauthorized Error', message);

export const ForbiddenError = (message: string) => new Error('403', 'Forbidden Error', message);

export const ConflictError = (message: string) => new Error('409', 'Conflict Error', message);

export const TimeoutError = (message: string) => new Error('408', 'Request Timeout', message);

export const BadRequestError = (message: string) => new Error('400', 'Bad Request', message);