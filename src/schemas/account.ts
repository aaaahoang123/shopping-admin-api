import {model, Schema} from "mongoose";
const idValidator = require('mongoose-id-validator-sync');
const bcrypt = require('bcrypt');
export const AccountsSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: [true, 'Username is required!'],
        minlength: [8, 'Username must have at least 8 characters!'],
        maxlength: [50, 'Username cannot longer than 50 characters!'],
        validate: {
            validator: (v: string) => {
                return /^[a-zA-Z0-9]+$/.test(v);
            },
            message: 'Username only have characters and numbers!'
        }
    },
    password: {
        type: String,
        required: [true, 'Password is required!'],
        minlength: [8, 'Password must have at least 8 characters!'],
        validate: {
            validator: (pass: string) => {
                return !pass.includes(' ');
            },
            message: 'Password must not contain space!'
        }
    },
    type: {
        type: Number,
        required: [true, 'Account must have a type!'],
        validate: {
            validator: (v: number) => {
                return [0, 1, 2].includes(v);
            },
            message: 'Account type must be 1: Customer, 2: Employee or 0: Admin!'
        },
        default: 1
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: [true, 'ID of user is required'],
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now
    },
    updated_at: {
        type: Date,
        required: true,
        default: Date.now
    },
    status: {
        type: Number,
        required: true,
        default: 1
    }
});

AccountsSchema.pre('save', function (next) {
    let user = this;
    // only hash the password if it has been modified (or is new)
    if (!this.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(Math.round(Math.random()*10), (err: any, salt: any) => {
        if (err) return next(err);

        // hash the password using our new salt
        // @ts-ignore
        bcrypt.hash(user.password, salt, (err: any, hash: any) => {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            // @ts-ignore
            user.password = hash;
            next();
        });
    });
});

/**
 * See more at: https://stackoverflow.com/questions/14588032/mongoose-password-hashing
 * @param candidatePassword
 */
AccountsSchema.methods.comparePassword = (candidatePassword: string): Promise<any> => {
    return bcrypt.compare(candidatePassword, this.password);
};

AccountsSchema.plugin(idValidator);

export const AccountsModel = model('accounts', AccountsSchema);