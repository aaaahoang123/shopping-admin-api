import { Schema, model } from "mongoose";

export const WareHouseSchema = new Schema({
    _id: {
        type: Schema.Types.ObjectId,
        required: [true, 'The id is required!'],
        ref: 'products'
    },
    qty: {
        type: Number,
        required: [true, 'Quantity is required!'],
        min: [0, 'Quantity can not lower than 1!']
    },
    last_update_by: {
        type: Schema.Types.ObjectId,
        required: [true, 'Must know who change this!'],
        ref: 'accounts'
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now
    },
    updated_at: {
        type: Date,
        required: true,
        default: Date.now
    },
    status: {
        type: Number,
        required: true,
        validate: {
            validator: (val: number) => {
                return [0, 1].includes(val);
            },
            message: 'Status must be 0 or 1!'
        },
        default: 1
    }
});

export const WareHouseModel = model('warehouse', WareHouseSchema);