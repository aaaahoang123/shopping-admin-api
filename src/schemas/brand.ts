import { Schema, model, Model } from "mongoose";

export const BrandsSchema: Schema = new Schema({
    name: {
        type: String,
        unique: true,
        required: [true, 'Name is required'],
        maxlength: [50, 'Name cannot longer than 50 characters!']
    },
    description: {
        type: String,
        required: [true, 'Description is required']
    },
    logo: {
        type: String,
        required: [true, 'Logo is required']
    },
    created_at: {
        type: Date,
        default: Date.now,
        required: true
    },
    updated_at: {
        type: Date,
        default: Date.now,
        required: true
    },
    status: {
        type: Number,
        default: 1,
        required: true
    }
});

export const BrandsModel: Model<any> = model('brands', BrandsSchema);