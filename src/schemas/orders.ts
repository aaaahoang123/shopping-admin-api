import { Schema, model } from "mongoose";

const idValidator = require('mongoose-id-validator-sync');

export const OrdersSchema = new Schema({
    products: [{
        _id: Schema.Types.ObjectId,
        unit_price: Number,
        qty: Number
    }],
    receiver_address: {
        type: String,
        required: [true, 'Address of receiver is required!']
    },
    receiver_city: {
        type: Number,
        required: [true, 'City of receiver is required!']
    },
    receiver_district: {
        type: Number,
        required: [true, 'District of receiver is required!']
    },
    receiver_ward: {
        type: Number,
        required: [true, 'Ward of receiver is required!']
    },
    receiver_phone: {
        type: String,
        required: [true, 'Phone number of receiver is required!'],
        validate: {
            validator: (pn: string) => {
                /**
                 * Match for: 
                 * - head: +84 or +(84), from 2 to 4 numbers
                 * - second: at least 3 numbers.
                 * - third: at least 3 numbers.
                 * - last: at least 1 numbers.
                 * - total: at least 7 numbers, not include head.
                 * - each part can split by -, space, ., or nothing
                 */
                return /^[\+]?[(]?[0-9]{2,4}[)]?[-\s\.*]?[0-9]{3,}?[-\s\.*]?[0-9]{3,}?[-\s\.*]?[0-9]{1,}$/im.test(pn);
            },
            message: (props: any) => `${props.value} is not a valid phone number!`
        }
    },
    receiver_email: {
        type: String,
        required: [true, 'Email of receiver is required!'],
        validate: {
            validator: (mail: string) => {
                return /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(mail);
            },
            message: (props: any) => `${props.value} is not a valid email!`
        }
    },
    total_price: Number,
    created_by: {
        type: Schema.Types.ObjectId,
        ref: 'accounts',
        required: true
    },
    updated_by: {
        type: Schema.Types.ObjectId,
        ref: 'accounts',
        required: true
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now
    },
    updated_at: {
        type: Date,
        required: true,
        default: Date.now
    },
    status: {
        type: Number,
        required: true,
        enum: [-1, 0, 1, 2],
        default: 0
    }
});

OrdersSchema.plugin(idValidator);

export const OrdersModel = model('orders', OrdersSchema);