import { Schema, model } from "mongoose";

export const WardsSchema = new Schema({
    "Type": Number,
    "SolrID": String,
    "ID": Number,
    "Title": String,
    "STT": Number,
    "TinhThanhID": Number,
    "TinhThanhTitle": String,
    "TinhThanhTitleAscii": String,
    "QuanHuyenID": Number,
    "QuanHuyenTitle": String,
    "QuanHuyenTitleAscii": String,
    "Created": String,
    "Updated": String
});

export const WardsModel = model('wards', WardsSchema);