import {Model, model, Schema} from "mongoose";
const idValidator = require('mongoose-id-validator-sync');

export const CategoriesSchema: Schema = new Schema({
    name: {
        type: String,
        required: [true, 'Name is required!']
    },
    handle: {
        type: String,
        unique: true,
        required: [true, 'Handle is required'],
        validate: {
            validator: (v: string) => /^[\w-_]*$/.test(v),
            message: `Handle just have characters, numbers, and '-' or '_'!`
        }
    },
    description: {
        type: String,
        required: [true, 'Description is required!']
    },
    level: {
        type: Number,
        required: [true, 'Category must have a level']
    },
    parent: {
        type: Schema.Types.ObjectId,
        ref: 'categories'
    },
    created_at: {
        type: Date,
        default: Date.now,
        required: true
    },
    updated_at: {
        type: Date,
        default: Date.now,
        required: true
    },
    status: {
        type: Number,
        required: true,
        default: 1
    }
});

CategoriesSchema.plugin(idValidator);

export const CategoriesModel: Model<any> = model('categories', CategoriesSchema);