import {Model, model, Schema} from "mongoose";

const idValidator = require('mongoose-id-validator-sync');
const bcrypt = require('bcrypt');

export const CredentialsSchema: Schema = new Schema({
    account_id: {
        type: Schema.Types.ObjectId,
        ref: 'accounts',
        required: true
    },
    token: {
        type: String,
        required: true,
        unique: true
    },
    type: {
        type: Number,
        required: true,
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now
    },
    expired_at: {
        type: Date,
        required: true,
        default: +new Date() + 7*24*60*60*1000
    },
    status: {
        type: Number,
        required: true,
        default: 1
    }
});

CredentialsSchema.pre('save', function (next) {
    let user = this;

    // generate a salt
    bcrypt.genSalt(Math.round(Math.random()*10), (err: any) => {
        if (err) return next(err);
        // @ts-ignore
        user.token = bcrypt.hashSync(user.token, Math.round(Math.random()*10));
        next();
    });
});

CredentialsSchema.plugin(idValidator);

export const CredentialsModel: Model<any> = model('credentials', CredentialsSchema);