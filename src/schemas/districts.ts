import { Schema, model } from "mongoose";

export const DistrictsSchema = new Schema({
    "Type": Number,
    "SolrID": String,
    "ID": Number,
    "Title": String,
    "STT": String,
    "TinhThanhID": Number,
    "TinhThanhTitle": String,
    "TinhThanhTitleAscii": String,
    "Created": String,
    "Updated": String
});

export const DistrictsModel = model('districts', DistrictsSchema);