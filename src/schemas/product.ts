import {model, Model, Schema, Collection, CollectionBase} from "mongoose";
const idValidator = require('mongoose-id-validator-sync');

export const ProductsSchema: Schema = new Schema({
    code: {
        type: String,
        unique: true,
        required: [true, 'Code is required!'],
        validate: {
            validator: (v: any) => {
                return /^\S+$/.test(v);
            },
            message: (prop:any) => `${prop.value} is not a valid product code!`
        }
    },
    name: {
        type: String,
        required: [true, 'Name is required!']
    },
    description: {
        type: String,
        required: [true, 'Description is required!']
    },
    categories: {
        type: [Schema.Types.ObjectId],
        ref: 'categories',
        required: [true, 'Categories is required!'],
        validate: {
            validator: (v: Schema.Types.ObjectId[]) => {
                return v.length > 0;
            },
            message: `Must have at least 1 categories!`
        }
    },
    brand: {
        type: Schema.Types.ObjectId,
        ref: 'brands',
        required: [true, 'Brand is required']
    },
    price: {
        type: Number,
        required: [true, 'Price is required!'],
        min: [1, 'Price must bigger than 0!']
    },

    specifications: Object,

    // updated_by: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     required: true
    // },

    images: {
        type: [String],
        required: [true, 'Images is required!'],
        validate: {
            validator: (v: String[]) => {
                return v.length > 0;
            },
            message: () => 'Must have at least 1 image!'
        }
    },
    created_at: {
        type: Date,
        default: Date.now,
        required: true
    },
    updated_at: {
        type: Date,
        default: Date.now,
        required: true
    },
    status: {
        type: Number,
        default: 1,
        required: true
    }
});

ProductsSchema.plugin(idValidator);

export const ProductsModel: Model<any> = model('products', ProductsSchema);