import {model, Schema} from "mongoose";

export const UsersSchema = new Schema({
    user_code: {
        type: String,
        required: true,
        unique: true,
        default: Date.now
    },
    full_name: {
        type: String,
        required: [true, 'Full name is required!']
    },
    city: {
        type: Number,
        required: [true, 'City is required!']
    },
    district: {
        type: Number,
        required: [true, 'District is required!']
    },
    ward: {
        type: Number,
        required: [true, 'Ward is required!']
    },
    address: {
        type: String,
        required: [true, 'Address is required!']
    },
    email: {
        type: String,
        required: [true, 'Email is required!'],
        minlength: [8, 'Email must have at least 8 character!'],
        validate: {
            validator: (mail: string) => {
                return /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(mail);
            },
            message: (props: any) => `${props.value} is not a valid email!`
        }
    },
    phone: {
        type: String,
        required: [true, 'Phone number is required!'],
        validate: {
            validator: (pn: string) => {
                /**
                 * Match for: 
                 * - head: +84 or +(84), from 2 to 4 numbers
                 * - second: at least 3 numbers.
                 * - third: at least 3 numbers.
                 * - last: at least 1 numbers.
                 * - total: at least 7 numbers, not include head.
                 * - each part can split by -, space, ., or nothing
                 */
                return /^[\+]?[(]?[0-9]{2,4}[)]?[-\s\.*]?[0-9]{3,}?[-\s\.*]?[0-9]{3,}?[-\s\.*]?[0-9]{1,}$/im.test(pn);
            },
            message: (props: any) => `${props.value} is not a valid phone number!`
        }
    },
    gender: {
        type: Number,
        required: [true, 'Gender is required'],
        validate: {
            validator: (gender: number) => {
                return [0, 1, 2].includes(gender);
            },
            message: 'Gender must be 1: Male, 2: Female or 0: Hidden!'
        }
    },
    birthday: {
        type: Date,
        required: [true, 'Birthday is required']
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now
    },
    updated_at: {
        type: Date,
        required: true,
        default: Date.now
    },
    status: {
        type: Number,
        required: true,
        enum: [-1, 1],
        default: 1
    }
});

UsersSchema.virtual('account', {
    ref: 'accounts', // The model to use
    localField: '_id', // Find people where `localField`
    foreignField: 'user', // is equal to `foreignField`
    // If `justOne` is true, 'members' will be a single doc as opposed to
    // an array. `justOne` is false by default.
    justOne: true
});

UsersSchema.virtual('city_detail', {
    ref: 'cities',
    localField: 'city',
    foreignField: 'ID',
    justOne: true
});

UsersSchema.virtual('district_detail', {
    ref: 'districts',
    localField: 'district',
    foreignField: 'ID',
    justOne: true
});

UsersSchema.virtual('ward_detail', {
    ref: 'wards',
    localField: 'ward',
    foreignField: 'ID',
    justOne: true
});

export const UsersModel = model('users', UsersSchema);