import { Schema, model } from "mongoose";

export const CitiesSchema = new Schema({
    "Type": Number,
    "SolrID": String,
    "ID": Number,
    "Title": String,
    "STT": Number,
    "Created": String,
    "Updated": String,
    "TotalDoanhNghiep": Number
});

export const CitiesModel = model('cities', CitiesSchema);