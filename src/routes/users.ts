import {Router} from "express";
import {AExpressRouter} from "../interfaces/a-express-router";
import {UsersAccountsController} from "../controllers/users-accounts";
import {CredentialsController} from "../controllers/credentials";

export class UsersRouter extends AExpressRouter {

    constructor() {
        super();
        this.router = Router();
        this.controller = new UsersAccountsController();
        this.routing();
    }

    routing(): void {
        this.router
            .use(CredentialsController.getCredential)
            .post('/', this.controller.doPost)
            .get('/', this.controller.doGet)
            .get('/:id', (<UsersAccountsController>this.controller).doGetOne)
            .delete('/', this.controller.doDelete)
            .delete('/:id', this.controller.doDelete)
            .put('/:id', (<UsersAccountsController>this.controller).doPutOne)
            .put('/', this.controller.doPut)
    }
}
