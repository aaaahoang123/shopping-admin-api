import {Request, Response, Router} from "express";
import {BrandsRouter} from "./brands-router";
import {CategoriesRouter} from "./categories-router";
import {AExpressRouter} from "../interfaces/a-express-router";
import {ProductsRouter} from "./products-router";
import {UsersRouter} from "./users";
import {CredentialsController} from "../controllers/credentials";
import { AccountsRouter } from "./accounts-router";
import { LocationsRouter } from "./locations-router";
import { OrdersRouter } from "./orders-router";

export class IndexRouter extends AExpressRouter {
    constructor() {
        super();
        this.router = Router();
        this.routing();
    }

    routing() {
        this.router
            .get("/", function(req: Request, res: Response) {
                res.render("index", { title: "Express" });
            })
            .post('/authentications', CredentialsController.doPost)
            .delete('/authentications', CredentialsController.doDelete)
            .use('/brands', new BrandsRouter().router)
            .use('/categories', new CategoriesRouter().router)
            .use('/products', new ProductsRouter().router)
            .use('/users', new UsersRouter().router)
            .use('/accounts', new AccountsRouter().router)
            .use('/locations', new LocationsRouter().router)
            .use('/orders', new OrdersRouter().router)
    }
}
