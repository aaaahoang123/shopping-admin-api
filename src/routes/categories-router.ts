import {Router} from "express";
import {CategoriesController} from "../controllers/categories";
import {AExpressRouter} from "../interfaces/a-express-router";

export class CategoriesRouter extends AExpressRouter {
    constructor() {
        super();
        this.controller = new CategoriesController();
        this.router = Router();
        this.router.delete('/', (<CategoriesController>this.controller).doDeleteMany)
        this.routing();
    }
}