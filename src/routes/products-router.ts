import {AExpressRouter} from "../interfaces/a-express-router";
import {Router} from "express";
import {ProductsController} from "../controllers/products";

export class ProductsRouter extends AExpressRouter {
    constructor() {
        super();
        this.router = Router();
        this.controller = new ProductsController();
        this.routing();
        this.router.delete('/', (<ProductsController>this.controller).doDeleteMany);
    }
}