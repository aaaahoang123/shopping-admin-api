import { AExpressRouter } from "../interfaces/a-express-router";
import { Router } from "express";
import { AccountsController } from "../controllers/accounts";
import { CredentialsController } from "../controllers/credentials";

export class AccountsRouter extends AExpressRouter {
    constructor() {
        super();
        this.router = Router();
        this.controller = new AccountsController();
        this.router.get('/:id', CredentialsController.getCredential);
        this.routing();
    }
}