import {Router} from "express";
import {BrandsController} from "../controllers/brands";
import {AExpressRouter} from "../interfaces/a-express-router";

export class BrandsRouter extends AExpressRouter {
    constructor() {
        super();
        this.controller = new BrandsController();
        this.router = Router();
        this.router.delete('/', (<BrandsController>this.controller).doDeleteMany);
        this.routing()
    }
}