import { AExpressRouter } from "../interfaces/a-express-router";
import { Router } from "express";
import { OrdersController } from "../controllers/orders";
import { CredentialsController } from "../controllers/credentials";

export class OrdersRouter extends AExpressRouter {
    constructor() {
        super();
        this.router = Router();
        this.controller = new OrdersController();
        this.router.use(CredentialsController.getCredential);
        this.routing();
    }
}