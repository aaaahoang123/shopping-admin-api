import { Router } from "express";
import { LocationsController } from "../controllers/locations";

export class LocationsRouter {
    router: Router;
    controller: LocationsController
    constructor() {
        this.router = Router();
        this.controller = new LocationsController();
        this.routing();
    }

    routing() {
        this.router
            .get('/cities', this.controller.getAllCities)
            .get('/districts', this.controller.getDistrictOfCity)
            .get('/wards', this.controller.getWardsOfDistricts);
    }
}