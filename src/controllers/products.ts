import {IController} from "../interfaces/i-controller";
import {Request, Response} from "express";
import {CategoriesModel} from "../schemas/category";
import {RestfulService} from "../utilities/restful-service";
import {ProductsModel} from "../schemas/product";
import {BrandsModel} from "../schemas/brand";
import {MongoosePipelineBuilder} from "../utilities/mongoose-pipeline-builder";
import { BadRequestError } from "../entities/error";
import * as mongoose from "mongoose";
import { CredentialsController } from "./credentials";

export class ProductsController implements IController {
    async doDelete(req: Request, res: Response): Promise<any> {
        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) {
            return;
        }
        const deleteResult = ProductsModel.findOneAndUpdate({_id: req.params.id}, {
            $set: {
                status: -1,
                updated_at: new Date()
            }
        }, {new: true}).exec();
        await RestfulService.getInstance('Product', false, false).doResponse(deleteResult, res);
    }

    async doDeleteMany(req: Request, res: Response): Promise<any> {
        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) {
            return;
        }
        if (!req.headers.body) {
            return res.status(400).json({errors: {bad_request: BadRequestError('Please send array id in headers.body!')}});
        }
        const deleteResult = ProductsModel.update({_id: {$in: (<string>req.headers.body).split(',')}}, {$set: {status: -1, updated_at: new Date()}}, {multi: true, new: true}).exec();
        await RestfulService.getInstance('Product', false, false).doResponse(deleteResult, res);
    }

    /**
     * - with get list, we will have the below query properties
     * - q: <string>: query string for search
     * - page: <number> page for pagination
     * - limit: <number> limit for pagination
     * - lookup: <string> 'true' to lookup brand and categories
     * - brand: <string> id of brand to query
     * - category: <string> id of category to query
     * - time_sort: [1, -1] sort the products by time
     * - price_sort: [1, -1] sort the products by price
     * - name_sort: [1, -1] sort the products by name
     * - code_sort: [1, -1] sort the products by code
     * @param req
     * @param res
     */
    async doGet(req: Request, res: Response): Promise<any> {
        const pipeline = new MongoosePipelineBuilder().search(req.query.q, ['code', 'name', 'description']).paginate(req.query).pipeline;
        const result = ProductsController.getListProduct(req.query, pipeline);
        await RestfulService.getInstance('Product', true, req.query).doResponse(result, res);
    }

    async doGetOne(req: Request, res: Response): Promise<any> {
        const query: any = {};
        query[req.query.findby==='id'?'_id':'code'] = req.params.id;
        const dao = ProductsModel.findOne(query);
        if (req.query.lookup === 'true') {
            dao.populate({path: 'categories', model: CategoriesModel}).populate({path: 'brand', model: BrandsModel});
        }
        const product = dao.exec();
        await RestfulService.getInstance('Product', false, false).doResponse(product, res);
    }

    async doPost(req: Request, res: Response): Promise<any> {

        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) {
            return;
        }

        let cates = null;
        try {
            cates = await CategoriesModel.find({_id: {$in: req.body.categories}}).exec();
            let set = new Set();
            cates.forEach(cate => {
                set.add(String(cate._id));
                if (cate.parent) {
                    set.add(String(cate.parent._id));
                    if (cate.parent.parent) set.add(String(cate.parent.parent._id));
                }
            });
            
            set.delete('undefined');
            req.body.categories = [...set];
        } catch(e) {
            console.log('Can not find categories of product!', e);
        }
        let createResult = ProductsModel.create(req.body);
        await RestfulService.getInstance('Product', false, false).doResponse(createResult, res);
    }

    async doPut(req: Request, res: Response): Promise<any> {
        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) {
            return;
        }
        const category = {...req.body};
        category.updated_at = new Date();
        const updateResult = ProductsModel.findOneAndUpdate({_id: req.params.id}, {$set: category}, {new: true}).exec();
        await RestfulService.getInstance('Product', false, false).doResponse(updateResult, res);
    }

    static async getListProduct(condition: any, pipeline: any[]): Promise<any> {
        if (condition.brand) {
            try {
                pipeline[0].$match.brand = new mongoose.Types.ObjectId(condition.brand);
            } catch (e) {
                let i = 1;
            }
        }
        if (condition.category) {
            try {
                pipeline[0].$match.categories = new mongoose.Types.ObjectId(condition.category);
            } catch (e) {
                let i = 1;
            }
        }
        if (condition.lookup === 'true') {
            pipeline.splice(pipeline.length - 1, 0, {
                $lookup: {
                    from: "categories",
                    localField: "categories",
                    foreignField: "_id",
                    as: "categories"
                }
            });
            pipeline.splice(pipeline.length - 1, 0, {
                $lookup: {
                    from: "brands",
                    localField: "brand",
                    foreignField: "_id",
                    as: "brand"
                }
            });
            pipeline.splice(pipeline.length - 1, 0, {
                $unwind: {
                    path: '$brand',
                    preserveNullAndEmptyArrays: true
                }
            });
        }
        let sortOrder = <any>{};

        if (condition.time_sort) sortOrder.created_at = Number(condition.time_sort);

        if (condition.price_sort) sortOrder.price = Number(condition.price_sort);

        if (condition.name_sort) sortOrder.name = Number(condition.name_sort);

        if (condition.code_sort) sortOrder.code = Number(condition.code_sort);

        if (Object.keys(sortOrder).length !== 0) {
            pipeline.splice(pipeline.length - 1, 0, {$sort: sortOrder});
        }

        return ProductsModel.aggregate(pipeline).exec();
    }
}