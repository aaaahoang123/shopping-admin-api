import { Request, Response } from "express";
import { CitiesModel } from "../schemas/city";
import { RestfulService } from "../utilities/restful-service";
import { DistrictsModel } from "../schemas/districts";
import { WardsModel } from "../schemas/wards";

export class LocationsController {
    async getAllCities(req: Request, res: Response) {
        const cities = CitiesModel.find({}).exec();
        await RestfulService.getInstance('Cities', false, false).doResponse(cities, res);
    }

    async getDistrictOfCity (req: Request, res: Response) {
        const districts = DistrictsModel.find({TinhThanhID: Number(req.query.city)}).exec();
        await RestfulService.getInstance('Districts', false, false).doResponse(districts, res);
    }

    async getWardsOfDistricts(req: Request, res: Response) {
        const wards = WardsModel.find({QuanHuyenID: Number(req.query.district)}).exec();
        await RestfulService.getInstance('Wards', false, false).doResponse(wards, res);
    }
}