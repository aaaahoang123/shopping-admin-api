import {Request, Response} from "express";
import {BrandsModel} from "../schemas/brand";
import {MongoosePipelineBuilder} from "../utilities/mongoose-pipeline-builder";
import {IController} from "../interfaces/i-controller";
import {RestfulService} from "../utilities/restful-service";
import { BadRequestError } from "../entities/error";
import { CredentialsController } from "./credentials";

export class BrandsController implements IController {

    async doPost(req: Request, res: Response): Promise<any> {
        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) {
            return;
        }
        let createResult = BrandsModel.create(req.body);
        await RestfulService.getInstance('Brand', false, false).doResponse(createResult, res);
    }

    async doGet(req: Request, res: Response): Promise<any> {
        const condition = req.query;
        const listBrands = await BrandsModel.aggregate(new MongoosePipelineBuilder().search(condition.q, ['description', 'name']).paginate(condition).pipeline).exec();
        await RestfulService.getInstance('Brand',true, condition).doResponse(listBrands, res);
    }

    async doGetOne(req: Request, res: Response): Promise<any> {
        const query: any = {};
        query[req.query.findby==='id'?'_id':'name'] = req.params.id;
        const brand = BrandsModel.findOne(query).exec();
        await RestfulService.getInstance('Brand', false, false).doResponse(brand, res);
    }

    async doDelete(req: Request, res: Response): Promise<any> {
        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) {
            return;
        }
        const deleteResult = BrandsModel.findOneAndUpdate({_id: req.params.id}, {$set: {status: -1, updated_at: new Date()}}, {new: true}).exec();
        await RestfulService.getInstance('Brand', false, false).doResponse(deleteResult, res);
    }

    async doDeleteMany(req: Request, res: Response) {
        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) {
            return;
        }
        if (!req.headers.body) {
            return res.status(400).json({errors: {bad_request: BadRequestError('Please send array id in headers.body!')}});
        }
        const deleteResult = BrandsModel
                                .update({_id: {$in: (<string>req.headers.body).split(',')}}, {$set: {status: -1, updated_at: new Date()}}, {multi: true, new: true})
                                .exec();
        await RestfulService.getInstance('Brands', false, false).doResponse(deleteResult, res);
    }

    async doPut(req: Request, res: Response): Promise<any> {
        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) {
            return;
        }
        let brand = {...req.body};
        brand.updated_at = new Date();
        const updateResult = BrandsModel.findOneAndUpdate({_id: req.params.id}, {$set: brand}, {new: true}).exec();
        await RestfulService.getInstance('Brand', false, false).doResponse(updateResult, res);
    }
}