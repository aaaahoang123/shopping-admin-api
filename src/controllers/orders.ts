import { IController } from "../interfaces/i-controller";
import { Request, Response } from "express";
import { UnauthorizedError } from "../entities/error";
import { OrdersModel } from "../schemas/orders";
import { RestfulService } from "../utilities/restful-service";

export class OrdersController implements IController {
    async doPost(req: Request, res: Response): Promise<any> {
        if (!res.locals.credential) {
            return res
                .status(401)
                .json({
                    errors: {
                        unauthorized: UnauthorizedError('You don\'t access to do this request. Please login first! If you have logged in, then may your token is expired!')
                    }
                });
        }

        const data = {...req.body};
        data.created_by = data.updated_by = res.locals.credential.account_id;
        const insertPromise = OrdersModel.create(data);
        await RestfulService.getInstance('Order', false, false).doResponse(insertPromise, res);
    }
    doPut(req: Request, res: Response): Promise<any> {
        throw new Error("Method not implemented.");
    }
    doDelete(req: Request, res: Response): Promise<any> {
        throw new Error("Method not implemented.");
    }
    doGetOne(req: Request, res: Response): Promise<any> {
        throw new Error("Method not implemented.");
    }
    doGet(req: Request, res: Response): Promise<any> {
        throw new Error("Method not implemented.");
    }
}