import {IController} from "../interfaces/i-controller";
import {Request, Response} from "express";
import {UsersModel} from "../schemas/user";
import {AccountsModel} from "../schemas/account";
import {RestfulService} from "../utilities/restful-service";
import {NotFoundError, UnauthorizedError, DataBaseError} from "../entities/error";
import { MongoosePipelineBuilder } from "../utilities/mongoose-pipeline-builder";
import { CredentialsController } from "./credentials";

const Transaction = require('mongoose-transactions');
const transaction = new Transaction();

export class UsersAccountsController implements IController {

    /**
     * - Check credential first, if not, reject the request.
     * - Account to delete is the credential account it self.
     * - If the params /:id has defined, account to delete is the params, but must check permission of credential.
     * - Delete the account and user by a transaction. 
     * @param req
     * @param res
     */
    async doDelete(req: Request, res: Response): Promise<any> {
        if (!res.locals.credential) {
            return res.status(401).json({errors: {unauthorized: UnauthorizedError('You don\'t access to do this request. Please login first! If you have logged in, then may your token is expired!')}});
        }

        let accountId = res.locals.credential.account_id;
        let userId;
        try {
            if (req.params.id) {
                if (![0,2].includes(res.locals.credential.type)) {
                    return res.status(403).json({errors: {unauthorized: UnauthorizedError('You don\'t access to do this request. You must have admin or employee account!')}});
                }
                const us: any = await UsersModel.findById(req.params.id).populate('account').exec();
                accountId = us.account._id;
                userId = us._id;
            } else {
                const account: any = await AccountsModel.findById(accountId).exec();
                accountId = account._id;
                userId = account.user;
            }
            
            transaction.update('accounts', accountId, {status: -1, updated_at: new Date()}, {new: true});
            transaction.update('users', userId, {status: -1, updated_at: new Date()}, {new: true});
            return await RestfulService.getInstance('UserAccount', false, false).doTransactionResponse(transaction, res);
        } catch (e) {
            console.log(e);
            res.status(500).json({errors: {database: DataBaseError('Database error, try again later!')}});
        }
       
    }

    /**
     * - Function to get user and account, we will has following property in req
     * - res.locals.credential: is a credential define by middleware of CredentialsController call by router
     * - data: ['list', null] If list, check credential of admin or employee, if not, reject request, if yes, get list account - user and return
     * - page: <number> Effected when data=list
     * - limit: <number> Effected when data=list
     * - lookup: ['true', null] If lookup === 'true', then the account will lookup the user.
     * - If data === 'list' and lookup, the list user will lookup account
     * @param req 
     * @param res 
     */
    async doGet(req: Request, res: Response): Promise<any> {
        if (!res.locals.credential) {
            return res.status(401).json({errors: {unauthorized: UnauthorizedError('You don\'t access to do this request. Please login first! If you have logged in, then may your token is expired!')}});
        }

        if (req.query.data === 'list') {
            if (![0,2].includes(res.locals.credential.type)) {
                return res.status(401).json({errors: {unauthorized: UnauthorizedError('You don\'t access to do this request. You must have admin or employee account!')}});
            }
            const listPipeline = new MongoosePipelineBuilder()
                                    .search(req.query.q, ['account.username', 'full_name', 'address', 'email', 'phone', 'birthday'])
                                    .paginate(req.query).pipeline;
            
            if (req.query.lookup === 'true') {
                listPipeline.unshift({$unwind: {path: '$account', preserveNullAndEmptyArrays:true}});
                listPipeline.unshift({
                    $lookup: {
                        from: "accounts",
                        localField: "_id",
                        foreignField: "user",
                        as: "account"
                    }
                });
            }
        
            return await RestfulService.getInstance('UserAccount', true, req.query).doResponse(UsersModel.aggregate(listPipeline).exec(), res);
        }

        let account: any = await AccountsModel.findById(res.locals.credential.account_id).exec();

        let user = UsersModel.findById(account.user);
        if (req.query.lookup === 'true') user = user.populate('account');
  
        return await RestfulService.getInstance('User', false, false).doResponse(user.exec(), res);
        
    }

    async doGetOne(req: Request, res: Response): Promise<any> {
        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) return;
        const query: any = {};
        query[req.query.findby==='id'?'id':'code'] = req.params.id;
        let user = UsersModel.findOne(query);
        if (req.query.lookup === 'true') user = user.populate('account');

        await RestfulService.getInstance('User', false, false).doResponse(user.exec(), res);
    }

    /**
     * - Use transaction to create account and users
     * - If have a credential, but not admin or employee, reject the request
     * - Then, if lookup === 'false', just only create an user
     * - Else, use transaction to create user and account.
     * @param req
     * @param res
     */
    async doPost(req: Request, res: Response): Promise<any> {
        const obj = {...req.body};

        if (res.locals.credential) {
            if (![0,2].includes(res.locals.credential.type)) {
                return res.status(401).json({errors: {unauthorized: UnauthorizedError('You don\'t access to do this request. You must be anonymous, or admin, employee account!')}});
            }
            if (req.query.lookup === 'false') {
                const user = new UsersModel(req.body).save();
                return await RestfulService.getInstance('User', false, false).doResponse(user, res);
            }
        }

        obj.user = transaction.insert('users', obj);
        transaction.insert('accounts', obj);
        await RestfulService.getInstance('UserAccount', false, false).doTransactionResponse(transaction, res);
    }

    /**
     * - Update own user data, auto read account id from credential
     * - req.query.lookup: ['true', null]: If true, update the user data and account data as well
     * @param req 
     * @param res 
     */
    async doPut(req: Request, res: Response): Promise<any> {
        if (!res.locals.credential) {
            return res.status(403).json({errors: {unauthorized: UnauthorizedError('You don\'t access to do this request. Please login first! If you have logged in, then may your token is expired!')}});
        }

        let accountId = res.locals.credential.account_id;
    
        let account: any = await AccountsModel.findById(accountId).exec();

        const obj = {...req.body, updated_at: new Date()};
        if (account !== null) {
            if (req.query.lookup === 'true') {
                transaction.update('accounts', accountId, obj, {new: true});
                transaction.update('users', account.user, obj, {new: true});
                return await RestfulService.getInstance('UserAccount', false, false).doTransactionResponse(transaction, res);
            }
            return await RestfulService.getInstance('User', false, false).doResponse(AccountsModel.findByIdAndUpdate(account.user, {$set: obj}).exec(), res);
        }
        res.status(404).json({errors: {not_found: NotFoundError('Cannot found account!')}});
    }

    /**
     * - Update an exactly user data. This function only enable for admin
     * - req.query.lookup: ['true', null]: If true, update the user data and account data as well
     * - req.params.id: user_code of the user
     * @param req 
     * @param res 
     */
    async doPutOne(req: Request, res: Response): Promise<any> {
        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) return;
        
        const user: any = await UsersModel.findOne({user_code: req.params.id}).populate('account').exec();
        const obj = {...req.body, updated_at: new Date()};
        if (user !== null) {
            if (req.query.lookup === 'true') {
                transaction.update('accounts', user.account._id, obj, {new: true});
                transaction.update('users', user._id, obj, {new: true});
                return await RestfulService.getInstance('UserAccount', false, false).doTransactionResponse(transaction, res);
            }
            return await RestfulService.getInstance('User', false, false).doResponse(UsersModel.findByIdAndUpdate(user._id, {$set: obj}, {new: true}).exec(), res);
        }
        res.status(404).json({errors: {not_found: NotFoundError('Cannot found user!')}});
    }

}