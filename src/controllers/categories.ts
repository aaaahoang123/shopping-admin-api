import {IController} from "../interfaces/i-controller";
import {Request, Response} from "express";
import {MongoosePipelineBuilder} from "../utilities/mongoose-pipeline-builder";
import {CategoriesModel} from "../schemas/category";
import {RestfulService} from "../utilities/restful-service";
import { BadRequestError } from "../entities/error";
import { CredentialsController } from "./credentials";

export class CategoriesController implements IController {

    /**
     * with get list, we will have the below query properties
     * q: <string>: query string for search
     * page: <number> page for pagination
     * limit: <number> limit for pagination
     * lookup: <string> 'true' to lookup all ref categories
     * level: <number> select all categories by number
     * @param req
     * @param res
     */
    async doGet(req: Request, res: Response): Promise<any> {
        const condition = req.query;
        await RestfulService
            .getInstance('Category', true, condition)
            .doResponse(CategoriesController.getListCategories(condition, new MongoosePipelineBuilder().search(condition.q, ['description', 'name']).paginate(condition).pipeline), res);
    }

    async doGetOne(req: Request, res: Response): Promise<any> {
        const query: any = {};
        query[req.query.findby==='id'?'_id':'handle'] = req.params.id;
        const category = CategoriesModel.findOne(query).exec();
        await RestfulService.getInstance('Category', false, false).doResponse(category, res);
    }

    async doPost(req: Request, res: Response): Promise<any> {
        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) {
            return;
        }
        let createResult = CategoriesModel.create(req.body);
        await RestfulService.getInstance('Category', false, false).doResponse(createResult, res);
    }

    async doPut(req: Request, res: Response): Promise<any> {
        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) {
            return;
        }
        const category = {...req.body};
        category.updated_at = new Date();
        const updateResult = CategoriesModel.findOneAndUpdate({_id: req.params.id}, {$set: category}, {new: true}).exec();
        await RestfulService.getInstance('Category', false, false).doResponse(updateResult, res);
    }

    async doDelete(req: Request, res: Response): Promise<any> {
        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) {
            return;
        }
        const deleteResult = CategoriesModel.findOneAndUpdate({_id: req.params.id}, {$set: {status: -1, updated_at: new Date()}}, {new: true}).exec();
        await RestfulService.getInstance('Category', false, false).doResponse(deleteResult, res);
    }

    async doDeleteMany(req: Request, res: Response) {
        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) {
            return;
        }
        if (!req.headers.body) {
            return res.status(400).json({errors: {bad_request: BadRequestError('Please send array id in headers.body!')}});
        }
        const deleteResult = CategoriesModel.update({_id: {$in: (<string>req.headers.body).split(',')}}, {$set: {status: -1, updated_at: new Date()}}, {multi: true, new: true}).exec();
        await RestfulService.getInstance('Categories', false, false).doResponse(deleteResult, res);
    }

    static async getListCategories(condition: any, pipeline: any[]): Promise<any> {
        if (condition.level && /^\d+$/.test(condition.level)) {
            pipeline[0].$match = {...pipeline[0].$match, ...{level: Number(condition.level)}};
        }
        if (condition.lookup === 'true') {
            pipeline.splice(pipeline.length - 1, 0, {
                $lookup: {
                    from: "categories",
                    let: {parent_id: "$_id"},
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        {$eq: ["$parent", "$$parent_id"]},
                                        {$eq: ["$status", 1]}
                                    ]
                                }
                            }
                        },
                        {
                            $lookup: {
                                from: "categories",
                                localField: "_id",
                                foreignField: "parent",
                                as: "children"
                            }
                        }
                    ],
                    as: "children"
                }
            });
            pipeline.splice(pipeline.length - 1, 0, {
                $lookup: {
                    from: "categories",
                    let: {parent_id: "$parent"},
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        {$eq: ['$_id', '$$parent_id']},
                                        {$eq: ['$status', 1]}
                                    ]
                                }
                            }
                        },
                        {
                            $lookup: {
                                from: "categories",
                                localField: "parent",
                                foreignField: "_id",
                                as: "parent"
                            }
                        }
                    ],
                    as: "parent"
                }
            });
        }
        return CategoriesModel.aggregate(pipeline).exec();
    }
}

