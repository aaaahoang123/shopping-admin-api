import { IController } from "../interfaces/i-controller";
import { Request, Response } from "express";
import { NotFoundError } from "../entities/error";
import { RestfulService } from "../utilities/restful-service";
import { AccountsModel } from "../schemas/account";
import { CredentialsController } from "./credentials";

export class AccountsController implements IController {
    async doGet(req: Request, res: Response): Promise<any> {
        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) {
            return;
        }
        if (!req.query.user) {
            return res.status(404).json({errors: {not_found: NotFoundError('Can not get any result without user ID!')}});
        }
        const account = AccountsModel.findOne({user: req.query.user}).exec();
        await RestfulService.getInstance('Account', false, false).doResponse(account, res);
    }

    async doPost(req: Request, res: Response): Promise<any> {

        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) {
            return;
        }

        const newAccount = new AccountsModel({...req.body}).save();
        await RestfulService.getInstance('Account', false, false).doResponse(newAccount, res);
    }

    async doPut(req: Request, res: Response): Promise<any> {
        
        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) {
            return;
        }

        const obj = {...req.body, updated_at: new Date()};
        await RestfulService.getInstance('Account', false, false).doResponse(AccountsModel.findByIdAndUpdate(req.params.id, {$set: obj}, {new: true}).exec(), res);
    }
    async doDelete(req: Request, res: Response): Promise<any> {
        res.status(404).json({errors: {resource: NotFoundError('Not Found')}});
    }
    async doGetOne(req: Request, res: Response): Promise<any> {
        if (!await CredentialsController.checkAdminCredentialAndReject(req, res)) {
            return;
        }
        const account = AccountsModel.findOne({username: req.params.id}).exec();
        await RestfulService.getInstance('Account', false, false).doResponse(account, res);
    }
}
