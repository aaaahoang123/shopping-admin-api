import {AccountsModel} from "../schemas/account";
import {Request, Response, NextFunction} from "express";
import {CredentialsModel} from "../schemas/credential";
import {DataBaseError, UnauthorizedError, ForbiddenError} from "../entities/error";
import { RestfulService } from "../utilities/restful-service";
const bcrypt = require('bcrypt');

export class CredentialsController {

    /**
     * When post to /authentications check username, password and make a new credential object
     * @param req
     * @param res
     */
    static async doPost(req: Request, res: Response): Promise<any> {
        try {
            const account: any = await AccountsModel.findOne({username: req.body.username}).exec();
            if (account === null) {
                return res.status(401).json({errors: {username: UnauthorizedError('Invalid account!')}})
            }
            const passwordIsMatch = await bcrypt.compare(req.body.password, account.password);

            if (!passwordIsMatch) {
                return res.status(401).json({errors: {password: UnauthorizedError('Password not match!')}})
            }

            const credential = new CredentialsModel({
                account_id: account._id,
                type: account.type,
                token: account._id.toString() + account.username + new Date().toDateString()
            }).save();
            await RestfulService.getInstance('Credential', false, false).doResponse(credential, res);
        } catch (e) {
            console.log(e);
            res.status(500).json({errors: {database: DataBaseError(e.message)}})
        }
    }

    static async doDelete(req: Request, res: Response): Promise<any> {
        const deleteResult = CredentialsModel.findOneAndUpdate({token: req.headers.authorization}, {$set: {status: -1}}, {new: true}).exec();
        await RestfulService.getInstance('Credential', false, false).doResponse(deleteResult, res);
    }

    /**
     * Middleware to get an credential, then set it in res.locals
     * @param req
     * @param res
     * @param next
     */
    static async getCredential(req: Request, res: Response, next: NextFunction): Promise<any> {
        try {
            const credential = await CredentialsModel.findOne({token: req.headers.authorization, status: 1}).exec();
            if (credential && credential.expired_at < new Date()) {
                CredentialsModel.findOneAndUpdate({token: req.headers.authorization}, {status: -1}, {new: true}, (err, result) => {
                    console.log('Deleted expire credential!');
                });
            } else {
                res.locals.credential = credential;
            }
        } catch (e) {
            console.log('Error when check credential!', e);
        } finally {
            next();
        }
    }

    /**
     * Check permission of an credential, and response error if it not existed, or wrong type
     * @param req 
     * @param res 
     * @param next
     */
    static async checkAdminCredentialAndReject(req: Request, res: Response): Promise<boolean> {
        const credential = res.locals.credential;

        if (!credential) {
            res.status(401).json({errors: {unauthorized: UnauthorizedError('You don\'t access to do this request. Please login first! If you have logged in, then may your token is expired!')}});
            return false;
        }

        if (![0,2].includes(credential.type)) {
            res.status(401).json({errors: {unauthorized: UnauthorizedError('You don\'t access to do this request. You must have admin or employee account!')}});
            return false;
        }

        return true;
    }

}