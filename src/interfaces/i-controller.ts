import {Request, Response} from "express";

/**
 * @interface interface of all express controller
 */
export interface IController {
    /**
     * Trigger when a request get send to '/' route
     * @param req
     * @param res
     */
    doGet(req: Request, res: Response): Promise<any>;

    /**
     * Trigger when a request post send to '/' route
     * @param req
     * @param res
     */
    doPost(req: Request, res: Response): Promise<any>;

    /**
     * Trigger when a request put send to '/:id' route
     * @param req
     * @param res
     */
    doPut(req: Request, res: Response): Promise<any>;

    /**
     * Trigger when a request delete send to '/:id' route
     * @param req
     * @param res
     */
    doDelete(req: Request, res: Response): Promise<any>;

    /**
     * Trigger when a request get send to '/:id' route
     * @param req
     * @param res
     */
    doGetOne(req: Request, res: Response): Promise<any>;
}