import {Router, Request, Response, NextFunction} from "express";
import {IController} from "./i-controller";
import { CredentialsController } from "../controllers/credentials";

export abstract class AExpressRouter {
    router: Router;
    controller: IController;

    routing() {
        this.router
            .use((req: Request, res: Response, next: NextFunction) => {
                if (req.method !== 'GET') {
                    CredentialsController.getCredential(req, res, next);
                } else {
                    next();
                }
            })
            .get('/', this.controller.doGet)
            .get('/:id', this.controller.doGetOne)
            .post('/', this.controller.doPost)
            .put('/:id', this.controller.doPut)
            .delete('/:id', this.controller.doDelete)
    }
}