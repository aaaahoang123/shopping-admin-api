# Clone

Run: git clone https://aaaahoang123@bitbucket.org/aaaahoang123/shopping-admin-client.git
Or: git clone https://gitlab.com/aaaahoang123/shopping-admin-api.git

## Client

[Bitbucket](https://aaaahoang123@bitbucket.org/aaaahoang123/shopping-admin-client/src/).
[Gitlab](https://gitlab.com/aaaahoang123/shopping-admin-client).

## Init

Run command: `npm install`.

## Compile and run

Run command: `npm start`.

## Note
Please install `nodemon`, `tcs`, `typescript`,... as global first!